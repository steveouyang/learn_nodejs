const url = require("url");

/* 服务端立场：解析请求url【协议 域名 端口 路径  请求参数 哈希#】 */
const reqUrl =
    // "https://www.baidu.com/s?wd=%E4%BD%A0%E5%A6%B9&rsv_spt=1&rsv_iqid=0x9343b62200013686&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_dl=tb&rsv_sug3=6&rsv_sug1=4&rsv_sug7=100&rsv_sug2=0&rsv_btype=i&prefixsug=%25E4%25BD%25A0%25E5%25A6%25B9&rsp=8&inputT=1048&rsv_sug4=1048#head";
    "https://127.0.0.1/s?wd=%E4%BD%A0%E5%A6%B9&rsv_spt=1&rsv_iqid=0x9343b62200013686&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_dl=tb&rsv_sug3=6&rsv_sug1=4&rsv_sug7=100&rsv_sug2=0&rsv_btype=i&prefixsug=%25E4%25BD%25A0%25E5%25A6%25B9&rsp=8&inputT=1048&rsv_sug4=1048#head";

/* 从URL中解析出各种信息 */
const parsedRet = url.parse(reqUrl);
console.log(parsedRet.protocol);
console.log(parsedRet.host);
console.log(parsedRet.hostname);
console.log(parsedRet.port);
console.log(parsedRet.path);
console.log(parsedRet.pathname);
console.log(parsedRet.query);
console.log(parsedRet.search);
console.log(parsedRet.hash);

/* 客户端立场 构造url */
console.log(url.resolve("http://www.baidu.com", "/s?w=你妹"));
