const path = require("path");

console.log(path.join(__dirname, "your", "sister"));
console.log(path.join(__dirname, "your", "../sister"));
console.log(path.join(__dirname, "your", "./sister"));

// join对所有子路径做暴力拼接
console.log(path.join("/foo", "/bar", "baz"));

// 自右向左做拼接 遇到/bar时 将/视为磁盘根目录（不再有上一级） 解析结束
console.log(path.resolve("/foo", "/bar", "baz"));

// 拼接路径 但每个路径 都以前一个路径为上下文 第一个参数的上下文是 __dirname
console.log(path.resolve("wwwroot", "static_files/png/", "../gif/image.gif"));
