const qs = require("querystring");

/* 我是客户端 我想构造请求参数 */
// var queryData = qs.stringify({
//     username: "admin",
//     password: "123456",
// });
// console.log(queryData);

/* 我是服务端 我需要解析用户的请求参数 */
// const queryStr = "username=admin&password=123456";
// const parsedKvs = qs.parse(queryStr);
// console.log(parsedKvs);

/* 去中文 还原中文 */
console.log(qs.escape("你妹"));
console.log(qs.unescape("%E4%BD%A0%E5%A6%B9"));
