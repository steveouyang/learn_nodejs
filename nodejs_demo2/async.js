var async = require("async");

console.time("test");

// 串行无关联
// async.series(
//     [
//         function (callback) {
//             setTimeout(function () {
//                 console.log("任务1完毕");

//                 // 告诉框架我的任务完成了 产出为"one"
//                 callback(null, "one");
//             }, 2000);
//         },

//         function (callback) {
//             setTimeout(function () {
//                 // 通过callback通知框架 我的任务执行完毕 产出结果为two
//                 callback({ err: "我爱睡觉" }, null);
//             }, 3000);
//         },

//         function (callback) {
//             setTimeout(function () {
//                 // 通过callback通知框架 我的任务执行完毕 产出结果为two
//                 callback(null, "three");
//             }, 1000);
//         },
//     ],

//     /* 任务组的总回调 */
//     function (err, results) {
//         console.log(err);
//         console.log(results); //[one,two]
//         console.timeEnd("test");
//     }
// );

// async.series(
//     {
//         one: function (callback) {
//             setTimeout(function () {
//                 callback(null, "1");
//             }, 1000);
//         },
//         two: function (callback) {
//             setTimeout(function () {
//                 callback(null, "2");
//             }, 2000);
//         },
//     },
//     function (err, results) {
//         console.log(results);
//         console.timeEnd("test");
//     }
// );

// 并行无关联
// async.parallel(
//     [
//         function (callback) {
//             setTimeout(function () {
//                 // 通知框架 我的任务已完成 产出为one
//                 callback(null, "one");
//             }, 2000);
//         },
//         function (callback) {
//             setTimeout(function () {
//                 // 通知框架 我的任务已完成 产出为two
//                 callback(new Error("我爱睡觉"), "two");
//             }, 3000);
//         },
//     ],
//     function (err, results) {
//         console.log(err);
//         console.log(results);
//         console.timeEnd("test");
//     }
// );

// 串行有关联
async.waterfall(
    [
        function (callback) {
            console.log("任务1");

            // 通知框架我的任务完成了 我的输出作为下一个任务的输入
            callback(null, ["one", 1], ["two", 2]);
        },

        function (arr1, arr2, callback) {
            console.log("任务2", arr1, arr2);

            // 通知框架我的任务完成了 我的输出作为下一个任务的输入
            callback(null, arr1, arr2, ["three", 3]);
        },

        function (arr1, arr2, arr3, callback) {
            console.log("任务3", arr1, arr2, arr3);

            // 通知框架我的任务完成了 我的输出作为任务组的总结果
            callback(null, [arr1, arr2, arr3, "done"]);
        },
    ],

    /* 任务组总回调 */
    function (err, results) {
        // 最后一个任务产出的结果作为总结果
        console.log("err=", err);
        console.log("results=", results);
    }
);
