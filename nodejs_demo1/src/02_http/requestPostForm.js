const http = require("http");

// 导入系统的querystring模块
const qs = require("querystring");

// const postData = "{'username':'admin','password':'54321'}"

/* 构造请求携带的表单数据 */
// 构造请求数据的querystring形式
// const postData = "username=nimei&pwd=123456";

// 将对象格式化为querystring形式
const postData = qs.stringify({
    username: "admin",
    password: "123456",
});
console.log(postData);

const options = {
    hostname: "www.httpbin.org",
    path: "/post",

    // 声明是POST请求
    method: "POST",

    // 配置请求头
    headers: {
        // 请求携带的数据格式
        "Content-Type": "application/x-www-form-urlencoded",

        // 计算请求数据的字节数
        "Content-Length": Buffer.byteLength(postData),
    },
};

const req = http.request(options, (res) => {
    let resData = "";
    let count = 0;

    res.on("data", (chunk) => {
        console.log(++count, chunk.toString());
        resData += chunk;
    });

    res.on("end", () => {
        console.log("响应完毕", resData);
    });

    // 监听响应的错误事件
    res.on("error", (err) => {
        console.log("res err=", err);
    });
});

req.on("error", (err) => {
    console.log("req err=", err);
});

// 在请求对象中携带数据
req.write(postData);

// 请求发送完毕
req.end();
