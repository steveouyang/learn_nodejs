// 引入https 准备向拉钩发起https请求
const https = require("https");

// 引入cheerio【npm i cheerio】 准备执行DOM操作
const cheerio = require("cheerio");

/* 向拉钩发起https请求 拿到其响应response */
https.get("https://www.lagou.com", (res) => {
    let htmlStr = "";

    /* 一桶一桶（缓冲区/Buffer）地读入数据 */
    res.on("data", (chunk) => {
        // 将每“块”数据拼接起来
        htmlStr += chunk;
    });

    /* 读取响应数据完毕 */
    res.on("end", () => {
        // console.log("html组装完毕", htmlStr);

        /* 对html字符串 爬之（DOM操作） */
        spiderIt(htmlStr);
    });

    res.on("error", (err) => {
        console.log("err=", err);
    });
});

const spiderIt = (htmlStr) => {
    // 将html载入到cheerio 获得其根布局
    const $ = cheerio.load(htmlStr);

    // [{title:"游戏",positions:['手游推广','页游推广','游戏主播']},{...}]
    const dataArr = [];

    /* 定位到所有的menu_box 显式地进行迭代 */
    $(".menu_box").each((index, element) => {
        // 找出一级标题
        const title = $(element).find("h2").text().trim();

        // ['手游推广','页游推广','游戏主播']
        const positions = [];

        /* 显式地遍历：当前行下的所有h3 */
        $(element)
            .find("h3")
            .each((i, e) => {
                // 将每个h3下的文本都丢入positions
                const position = $(e).text().trim();
                positions.push(position);
            });

        // 将爬到的当前行信息丢入dataArr
        dataArr.push({ title, positions });
    });

    console.log(dataArr);
};
