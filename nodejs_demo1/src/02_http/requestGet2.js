const https = require("https");

// http.get("https://v.juhe.cn/todayOnhistory/queryEvent.php?key=7459d5bf4ba9a4d042932f68990292ed&date=1/1", (res) => {
https.get("https://www.baidu.com", (res) => {
    let htmlStr = "";
    let count = 0;

    res.on("data", (chunk) => {
        console.log(++count, chunk);
        htmlStr += chunk;
    });

    res.on("end", () => {
        console.log("请求完毕", htmlStr);
    });

    res.on("error", (err) => {
        console.log("err=", err);
    });
});
