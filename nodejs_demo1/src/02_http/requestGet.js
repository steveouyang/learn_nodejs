const http = require("http");

// http://v.juhe.cn/todayOnhistory/queryEvent.php?key=7459d5bf4ba9a4d042932f68990292ed&date=1/1
var options = {
    hostname: "v.juhe.cn",
    port: 8123,
    method: "GET",
    path: "/todayOnhistory/queryEvent.php?key=7459d5bf4ba9a4d042932f68990292ed&date=8/8",
    timeout: 1000,
};

// 预备接收响应数据
var responseData = "";

// 记录数分批加载的序号
var count = 0;

/* 构造http请求对象 */
var request = http.request(options, (response) => {
    // console.log(response)
    console.log(response.statusCode);
    console.log(response.headers);

    // 设置响应内容的编码
    response.setEncoding("utf8");

    /* 分批从响应中读取数据内容 */
    response.on("data", (chunk) => {
        console.log("收到响应数据", ++count, chunk);
        responseData += chunk;
    });

    /* 响应数据传送完毕 */
    response.on("end", () => {
        console.log("数据接收完毕", responseData);
    });
});

/* 监听请求的错误事件 */
request.on("error", (error) => {
    console.error("error=", error);
});

/* 发送请求 */
request.end();
