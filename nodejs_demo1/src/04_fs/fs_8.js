const path = require("path");
const { rmDirRecursively } = require("./file_util");

const dirPath = path.join(__dirname, "logs"); //拼接路径
rmDirRecursively(dirPath);
