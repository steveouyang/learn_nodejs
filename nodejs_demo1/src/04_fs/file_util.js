const fs = require("fs");
const path = require("path");

/* 删除一个目录下的所有文件 */
function rmDirRecursively(dirPath) {
    // 读取文件夹下的全部文件名
    const files = fs.readdirSync(dirPath);
    console.log(files);

    // 删除所有文件
    files.forEach((file) => {
        const filepath = path.join(dirPath, file);
        console.log("filepath", filepath);

        if (fs.statSync(filepath).isFile()) {
            fs.rmSync(filepath);
        } else {
            // file是一个文件夹
            rmDirRecursively(filepath);
        }
    });

    // 删除空目录
    fs.rmdir(dirPath, (err) =>
        err ? console.log("删除失败", err) : console.log("删除目录成功")
    );
}

module.exports = {
    rmDirRecursively,
};
