// const exportedObj = require("events")
const EventEmitter = require("events");

class Player extends EventEmitter {}
const player = new Player();

player.on("play", function (data) {
    console.log(`开始播放${data}`);
});

setTimeout(() => {
    player.emit("play", "望月怀远");
}, 3000);
