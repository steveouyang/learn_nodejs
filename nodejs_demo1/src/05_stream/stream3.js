const fs = require("fs");
const { pipeline } = require("stream");
const zlib = require("zlib");

const gzTransformer = zlib.createGzip();

// fs.createReadStream("./logs/data.json")
//     .pipe(gzTransformer)
//     .pipe(fs.createWriteStream("./logs/data.json.gz"));

pipeline(
    fs.createReadStream("./logs/data.json"),
    gzTransformer,
    fs.createWriteStream("./logs/data.json.gz"),
    (err) => {
        if (!err) {
            console.log("管路已达终点");
        } else {
            console.log("err", err);
        }
    }
);

console.log("程序结束?");
