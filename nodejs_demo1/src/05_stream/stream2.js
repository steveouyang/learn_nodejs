// const fs = require("fs")
// const rs = fs.createReadStream("./logs/meizi.jpg")
// rs.pipe(
//     fs.createWriteStream("./logs/meizi2.jpg")
// )

const fs = require("fs");
const { pipeline } = require("stream");

const rs = fs.createReadStream("./logs/meizi.jpg");
const ws = fs.createWriteStream("./logs/meizi2.jpg");

pipeline(rs, ws, (err, value) => {
    console.log("管路已达终点");
    console.log(err);
    console.log(value);
});

rs.on("data", (chunk) => {
    console.log("收到数据", chunk);
});

rs.on("end",()=>{
    console.log("文件拷贝完毕");
});

rs.on("error",(err)=>{
    console.log("发生错误",err);
})

console.log("程序结束？");
